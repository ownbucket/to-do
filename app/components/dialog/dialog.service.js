/**
 * DialogService class.
 */
class DialogService {

  /**
   * @description Constructor for DialogService.
   * @param {$mdDialog} $mdDialog Angular Materials $mdDialog service
   * @param {CloudService} cloudService CloudService
   * @param {Object} $log Angular log service
   */
  constructor($mdDialog, cloudService, $log) {
    this.$mdDialog_ = $mdDialog;
    this.cloudService_ = cloudService;
    this.$log_ = $log;
  }

  /**
   * Shows a Prompt Dialog.
   */
  newTask(ev) {
    this.$mdDialog_.show({
      template: '<div>hi</div>',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
    .then(result => this.$log_(result))
    .catch(response => this.$log_(response));
  }

}

DialogService.$inject = ['$mdDialog', 'CloudService', '$log'];

export default DialogService;
