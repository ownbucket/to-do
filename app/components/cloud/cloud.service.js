import { AppData } from '../../config/app.data';
/**
 * CloudService class.
 */
class CloudService {

  /**
   * @description Constructor for CloudService.
   * @param {Object} $state angular $state service
   * @param {Object} $window angular $window service
   * @param {Object} $q angular $q service
   */
  constructor($state, $window, $q) {
    this.$state_ = $state;
    this.$q_ = $q;
    this.gApi_ = $window.gapi || {};
    this.gAuth_ = this.gApi_.auth2;
    this.gTask =
    this.allApis_ = false;
  }

  /**
   * Initializes Apis
   */
  init() {
    return this.initClient_().then(() => this.$q_.when(this.allApis_ = true));
  }

  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  initClient_() {
    const initClientSettings = {
      discoveryDocs: AppData.apiSettings.DISCOVERY_DOCS,
      clientId: AppData.apiSettings.CLIENT_ID,
      scope: AppData.apiSettings.SCOPES
    };
    return this.gApi_.client.init(initClientSettings);
  }

  /**
   * Is the user signed in.
   */
  isUserLogged() {
    return this.gAuth_.getAuthInstance().isSignedIn.get();
  }

  /**
   * Signs in.
   */
  signIn() {
    return this.gAuth_.getAuthInstance().signIn();
  }

  /**
   * Singns out.
   */
  signOut() {
    return this.gAuth_.getAuthInstance().signOut();
  }

  isApiInitialized_(promise) {
    return !this.allApis_ ? this.init().then(() => promise()) : promise();
  }

  /* ----------- Lists */

  /**
   * Get available lists.
   */
  getLists() {
    const reqWrapper = () => this.gApi_.client.tasks.tasklists.list();
    return this.isApiInitialized_(reqWrapper);
  }

  /**
   * Gets list.
   */
  getListById(listId) {
    const reqWrapper =
        () => this.gApi_.client.tasks.tasklists.get({'tasklist': listId});
    return this.isApiInitialized_(reqWrapper);
  }

  /**
   * Get available lists.
   */
  insertList(list) {
    const reqWrapper =
        () => this.gApi_.client.tasks.tasklists.insert({title: list.title});
    return this.isApiInitialized_(reqWrapper);
  }

  /**
   * Updates lists.
   */
  updateList(list) {
    return this.gApi_.client.tasks.tasklists.update(list);
  }

  /**
   * Deletes lists.
   */
  deleteList(list) {
    return this.gApi_.client.tasks.tasklists.delete({'tasklist': list.id});
  }

  /* ---------- Tasks */

  /**
   * Gets list.
   */
  getListOfTasks(listId) {
    const reqWrapper =
        () => this.gApi_.client.tasks.tasks.list({'tasklist': listId});
    return this.isApiInitialized_(reqWrapper);
  }
}

CloudService.$inject = ['$state', '$window', '$q'];

export default CloudService;
