import CloudService from './cloud/cloud.service';
import DialogService from './dialog/dialog.service';

/**
 * @ngdoc overview
 * @name ComponentsModule
 * @description
 *
 * The `ComponentsModule` module bundles all the components in the application
 * that are intended to reuse in another app.
 */
export const ComponentsModule =
  angular
    .module('components', [])
    .service('CloudService', CloudService)
    .service('DialogService', DialogService)
    .name;
