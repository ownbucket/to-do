
/**
 * AppConfig class to config the application.
 */
class AppConfig {

  /**
   * @description Constructor for AppConfig.
   * @param {Object} $urlRouterProvider $urlRouter provider
   * @param {Object} $qProvider $q provider
   */
  constructor($urlRouterProvider, $qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    $urlRouterProvider.otherwise('/todo');
  }
}

const AppConfigFactory = ($urlRouterProvider, $qProvider) => new AppConfig(
  $urlRouterProvider, $qProvider);

AppConfigFactory.$inject = ['$urlRouterProvider', '$qProvider'];

export default AppConfigFactory;
