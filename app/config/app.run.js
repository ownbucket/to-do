/**
 * AppRun class.
 */
class AppRun {
  /**
   * Constructor for the AppRun
   * @param {Object} $mdToast Materials desing toast service
   * @param {Object} $rootScope Angular scope service
   * @param {Object} $state Angular state service
   * @param {Object} $log Angular log service
   * @param {CloudService} cloudService CloudService
   */
  constructor($mdToast, $rootScope, $state, $log, cloudService ) {
    this.$mdToast_ = $mdToast;
    this.$rootScope_ = $rootScope;
    this.$state_ = $state;
    this.$log_ = $log;
    this.cloudService_ = cloudService;
  }

  /**
   * Initialize settings for state change.
   */
  init() {
    this.stateChangeSettings_();
  }

  /**
   * Launches state changes configuration.
   */
  stateChangeSettings_() {
    const stateChangeStart = '$stateChangeStart';

    const stateChangeStartHandler =
        (event, toState) => this.stateChangeStartHandler(event, toState);

    this.$rootScope_.$on(stateChangeStart, stateChangeStartHandler);

  }

  /**
   * Handler for redirection errors
   * @param {Object} event Event data
   * @param {Object} toState Destination state
   */
  stateChangeStartHandler(event, toState) {
    if (!this.cloudService_.clientInitialized) {
      this.cloudService_.init().then(() => this.redirectState_(event, toState));
    } else {
      this.redirectState_(event, toState);
    }
  }

  /**
   * Redirects State.
   * @param {Object} event Event data
   * @param {Object} toState Destination state
   */
  redirectState_(event, toState) {
    const userLogged = this.cloudService_.isUserLogged();
    const toLogin = toState.name === 'login';
    const toMain = toState.name === 'main';
    const tologgedAccess =
        toState.name === 'todo' || toState.name === 'me';
    if (toLogin && userLogged) {
      event.preventDefault();
      this.$state_.go('todo');
    } else if (tologgedAccess && !userLogged) {
      event.preventDefault();
      this.$state_.go('login');
    } else if (toMain) {
      this.$state_.go('todo');
    }
  }

}

/**
 * Creates an AppRun object an run the initial configuration for the app
 * @param {Object} $mdToast Materials desing toast service
 * @param {Object} $rootScope Angular root scope service
 * @param {Object} $state Angular state service
 * @param {Object} $log Angular log service
 * @param {CloudService} cloudService CloudService
 * @returns {Object} AppRun instance
 */
const AppRunFactory = ( $mdToast, $rootScope, $state, $log, cloudService ) => {
  const appRun = new AppRun( $mdToast, $rootScope, $state, $log, cloudService );
  appRun.init();
  return appRun;
}

AppRunFactory.$inject = ['$mdToast', '$rootScope', '$state', '$log', 'CloudService'];

export default AppRunFactory;
