export const AppData = {
  apiSettings: {
    // Client ID and API key from the Developer Console
    CLIENT_ID: '552987915827-ad8jsco98fnln7b2on1vudonndabrgok.apps.googleusercontent.com',
    // Array of API discovery doc URLs for APIs used by the quickstart
    DISCOVERY_DOCS: ["https://www.googleapis.com/discovery/v1/apis/tasks/v1/rest"],
    // Authorization scopes required by the API; multiple scopes can be
    // included, separated by spaces.
    SCOPES: 'https://www.googleapis.com/auth/tasks'
  }
};
