import { TaskController } from './task.controller';

/**
 * Task component.
 */
class TaskComponent {
  /**
   * Constructor for Task component
   */
  constructor() {
    this.templateUrl = 'todo/task/task.html';
    this.controller = TaskController;
    this.bindings = {
      task: '<'
    };
  }
}

export default new TaskComponent();
