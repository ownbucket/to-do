/**
 * Task controller.
 */
class TaskController {
  /**
   * Constructor for Task controller.
   * @param {Object} $state angular $state service
   */
  constructor($state) {
    this.$state = $state;
    this.activeFilter;
  }
}

TaskController.$inject = [ '$state' ];

export { TaskController };
