import { ViewerController } from './viewer.controller';

/**
 * Viewer component.
 */
class ViewerComponent {
  /**
   * Constructor for Viewer component
   */
  constructor() {
    this.templateUrl = 'todo/viewer/viewer.html';
    this.controller = ViewerController;
    this.bindings = {
      list: '<'
    };
  }
}

export default new ViewerComponent();
