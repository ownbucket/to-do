/**
 * Viewer controller.
 */
class ViewerController {
  /**
   * Constructor for Viewer controller.
   * @param {Object} $state angular $state service
   */
  constructor($state) {
    this.$state = $state;
  }
}

ViewerController.$inject = [ '$state' ];

export { ViewerController };
