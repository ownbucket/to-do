import {TodoConstants} from './todo.constants';
import { List } from './list/list.entity';

/**
 * TodoService class.
 */
class TodoService {

  /**
   * @description Constructor for TodoService.
   * @param {Object} $state angular $state service
   * @param {Object} $q angular $q service
   * @param {CloudService} cloudService CloudService
   */
  constructor($state, $q, cloudService) {
    this.$state_ = $state;
    this.$q_ = $q;
    this.cloudService_ = cloudService;
  }

  /**
   * Returns all the available filters.
   * @returns {Array<Object>}
   */
  getFilters() {
    return this.$q_.when(TodoConstants.filters);
  }

  /**
   * Returns lists.
   * @returns {Array<List>}
   */
  getLists() {
    return this.cloudService_.getLists()
        .then(response => _.map(response.result.items, item => new List(item)));
  }

}

TodoService.$inject = ['$state', '$q', 'CloudService'];

export default TodoService;
