import { TodoController } from './todo.controller';

/**
 * Todo component.
 */
class TodoComponent {
  /**
   * Constructor for Todo component
   */
  constructor() {
    this.templateUrl = 'todo/todo.html';
    this.controller = TodoController;
    this.bindings = {
      filters: '<',
      lists: '<'
    };
  }
}

export default new TodoComponent();
