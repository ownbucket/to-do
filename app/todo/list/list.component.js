import { ListController } from './list.controller';

/**
 * List component.
 */
class ListComponent {
  /**
   * Constructor for List component
   */
  constructor() {
    this.templateUrl = 'todo/list/list.html';
    this.controller = ListController;
    this.bindings = {
      lists: '<'
    };
  }
}

export default new ListComponent();
