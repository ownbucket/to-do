import ListService from './list.service';
import ListComponent from './list.component';
import ListRowComponent from './list-row/list-row.component';

/**
 * @ngdoc overview
 * @name ListModule
 * @requires ui.router
 * @description
 *
 * The `ListModule` bunldes the list features in the app.
 */
export const ListModule =
  angular
  .module('list', [])
  .component('list', ListComponent)
  .component('listRow', ListRowComponent)
  .service('ListService', ListService)
  .name;
