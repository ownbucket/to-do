import { List } from './list.entity';

/**
 * List controller.
 */
class ListController {
  /**
   * Constructor for List controller.
   * @param {Object} $state angular $state service
   * @param {Object} $timeout angular $timeout service
   * @param {ListService} listService ListService
   * @param {Object} $stateParams angular $stateParams service
   */
  constructor($state, $timeout, listService, $stateParams) {
    this.$state_ = $state;
    this.$timeout_ = $timeout;
    this.listService_ = listService;
    this.$stateParams_ = $stateParams;
    this.activeListId;
    this.newList;
    this.updatingList;
  }

  /**
   * $onInit component.
   */
  $onInit() {
    this.openList_();
  }

  /**
   * Opens list for default or according to the parameters in the url
   */
  openList_() {
    if (this.$stateParams_.listId) {
     this.activeListId = this.$stateParams_.listId;
    } else {
     this.activeListId = _.first(this.lists).id
     this.showTasks(this.activeListId);
    }
  }

  /**
   * Shows tasks.
   */
   showTasks(listId, reload = false) {
     this.updatingList = undefined;
     this.activeListId = listId;
     this.$state_.go('todo.viewer', {'listId': listId}, {reload: reload});
   }

   /**
    * Creates list
    */
   createNewList() {
     this.newList = this.newList ? undefined : new List();
   }

   /**
    * Inserts new list
    * @param {Object} list
    */
   insertList(list) {
     this.listService_.insertList(list)
      .then(response => {
        this.$timeout_(() => {
          this.newList = undefined;
          this.lists.push(new List(response.result));
        });
      });
   }

   /**
    * Updates lists.
    */
   updateList(list) {
     this.listService_.updateList(list)
        .then(() => {
          this.$timeout_(() => {
            this.updatingList = undefined;
            this.activeListId = list.id;
            this.showTasks(this.activeListId, true);
          });
        });
   }

   /**
    * Delete lists.
    */
   deleteList(list) {
     this.listService_.deleteList(list)
        .then(() => this.actionsAfterDeletingList_(list));
   }

   /**
    * Executes necessary actions after deleting a list.
    */
   actionsAfterDeletingList_(listToDelete) {
     this.$timeout_(() => {
       this.lists = _.filter(this.lists, list => {
         return list.id !== listToDelete.id;
       });
       this.activeListId = _.first(this.lists).id;
       this.showTasks(this.activeListId);
     });
   }
}

ListController.$inject =
    [ '$state', '$timeout', 'ListService', '$stateParams' ];

export { ListController };
