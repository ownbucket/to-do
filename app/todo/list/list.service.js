import { List } from './list.entity';

/**
 * ListService class.
 */
class ListService {

  /**
   * @description Constructor for ListService.
   * @param {Object} $state angular $state service
   * @param {Object} $q angular $q service
   * @param {CloudService} cloudService CloudService
   */
  constructor($state, $q, cloudService) {
    this.$state_ = $state;
    this.$q_ = $q;
    this.cloudService_ = cloudService;
  }

  /**
   * Returns list.
   * @returns {Array<List>}
   */
  getListById(listId) {
    let list;
    return this.cloudService_.getListById(listId)
        .then(response => {
          list  = new List(response.result);
          return this.cloudService_.getListOfTasks(response.result.id);
        })
        .then(response => {
          list.tasks = response.result.items;
          return list;
        });
  }

  /**
   * Inserts new list
   */
   insertList(list) {
     return this.cloudService_.insertList(list);
   }

   /**
    * Updates list
    */
    updateList(list) {
      const tasklist = {
        "tasklist": list.id,
        "kind": list.kind,
        "id": list.id,
        "etag": list.etag,
        "title": list.title,
        "updated": list.updated,
        "selfLink": list.selfLink
      };
      return this.cloudService_.updateList(tasklist);
    }

    /**
     * Deletes list
     */
     deleteList(list) {
       return this.cloudService_.deleteList(list);
     }

}

ListService.$inject = ['$state', '$q', 'CloudService'];

export default ListService;
