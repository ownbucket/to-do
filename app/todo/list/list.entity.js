/**
 * List entity.
 */
export class List {
  /**
   * Constructor for List class.
   * @param {Object} [{}] item
   */
  constructor(item = {}, tasks = []) {
    this.id = item.id;
    this.kind = item.kind;
    this.selfLink = item.selfLink;
    this.title = item.title;
    this.updated = item.updated;
    this.tasks = tasks;
    this.etag = item.etag;
  }
}
