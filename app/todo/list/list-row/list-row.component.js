/**
 * ListRow component.
 */
class ListRowComponent {
  /**
   * Constructor for ListRow component
   */
  constructor() {
    this.templateUrl = 'todo/list/list-row/list-row.html';
    this.bindings = {
      list: '<'
    };
  }
}

export default new ListRowComponent();
