/**
 * Todo controller
 */
class TodoController {
  /**
   * Constructor for Todo controller
   * @param {Object} $state angular $state service
   * @param {DialogService} dialogService DialogService
   */
  constructor($state, dialogService) {
    this.$state = $state;
    this.dialogService_ = dialogService;
    this.showSideNav = false;
  }

  /**
   * New task.
   */
  newTask(ev) {
    this.dialogService_.newTask(ev);
  }
}

TodoController.$inject = [ '$state', 'DialogService' ];

export { TodoController };
