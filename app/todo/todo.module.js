import TodoRoutes from './todo.routes';
import TodoService from './todo.service';
import TodoComponent from './todo.component';
import FilterComponent from './filter/filter.component';
import TaskComponent from './task/task.component';
import ViewerComponent from './viewer/viewer.component';
import {ListModule} from './list/list.module';

/**
 * @ngdoc overview
 * @name TodoModule
 * @requires ui.router
 * @requires ListModule
 * @description
 *
 * The `TodoModule` module bundles all the components in the todo application.
 */
export const TodoModule =
  angular
  .module('todo', [
    'ui.router',
    ListModule
  ])
  .component('todo', TodoComponent)
  .component('filter', FilterComponent)
  .component('task', TaskComponent)
  .component('viewer', ViewerComponent)
  .service('TodoService', TodoService)
  .config(TodoRoutes)
  .name;
