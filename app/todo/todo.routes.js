/**
 * Todo class.
 */
class TodoRoutes {
  /**
   * Constructor for TodoRoutes.
   * @param {Object} $stateProvider angular $state provider
   */
  constructor($stateProvider) {
    $stateProvider
      .state('todo', {
        url: '^/todo',
        template: `<todo
                    class="todo-flex-column"
                    filters="$resolve.filters"
                    lists="$resolve.lists">
                   </todo>`,
        resolve: {
          filters: ['TodoService', TodoService => TodoService.getFilters()],
          lists: ['TodoService', TodoService => TodoService.getLists()]
        }
      })
      .state('todo.viewer', {
        url: '/list?listId',
        views: {
          'viewer': {
            template: `<viewer
                        list="$resolve.list">
                       </viewer>`
                     }
        },
        resolve: {
          list: ['$stateParams','ListService', ($stateParams, ListService) =>
              ListService.getListById($stateParams.listId)]
        }
      });
  }
}

const TodoRoutesFactory = $stateProvider => new TodoRoutes($stateProvider);

TodoRoutesFactory.$inject = ['$stateProvider'];

export default TodoRoutesFactory;
