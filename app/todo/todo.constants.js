export const TodoConstants = {
  filters: [{
    icon: 'inbox',
    label: 'All',
    sort: ''
  }, {
    icon: 'face',
    label: 'Custome',
    sort: ''
  }, {
    icon: 'date_range',
    label: 'Date',
    sort: ''
  }, {
    icon: 'offline_pin',
    label: 'Done',
    sort: ''
  }, {
    icon: 'delete',
    label: 'Trash',
    sort: ''
  }]
}
