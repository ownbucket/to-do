import { FilterController } from './filter.controller';

/**
 * Filter component.
 */
class FilterComponent {
  /**
   * Constructor for Filter component
   */
  constructor() {
    this.templateUrl = 'todo/filter/filter.html';
    this.controller = FilterController;
    this.bindings = {
      filter: '<'
    };
  }
}

export default new FilterComponent();
