/**
 * Filter controller.
 */
class FilterController {
  /**
   * Constructor for Filter controller.
   * @param {Object} $state angular $state service
   */
  constructor($state) {
    this.$state = $state;
    this.activeFilter;
  }

  /**
   * On selects filter action.
   * @param {Object} filter Filter Object
   */
  onSelectFilter(filter) {
    this.$state.go('currentPath' + '?filter=' + filter.type)
      .then(() => this.activeFilter = filter);
  }
}

FilterController.$inject = [ '$state' ];

export { FilterController };
