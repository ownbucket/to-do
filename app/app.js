import AppConfig from './config/app.config';
import AppRun from './config/app.run';
import { ComponentsModule } from './components/components.module';
import { CommonsModule } from './commons/commons.module';
import { TodoModule } from './todo/todo.module';
import { MeModule } from './me/me.module';
import './styles/index.scss';

/**
 * @ngdoc overview
 * @name AppModule
 * @requires ComponentsModule
 * @requires CommonsModule
 * @requires TodoModule
 * @requires MeModule
 * @requires ngMessages
 * @requires ngMaterial
 * @requires ui.router
 * @description
 *
 * The `AppModule` module bundles the whole application.
 */
export const AppModule =
  angular
    .module('app', [
      ComponentsModule,
      CommonsModule,
      TodoModule,
      MeModule,
      'ngMessages',
      'ngMaterial',
      'ui.router'
    ])
    .config(AppConfig)
    .run(AppRun)
    .name;
