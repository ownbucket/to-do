import { LoginController } from './login.controller';

/**
 * Login Component.
 */
class LoginComponent {
  /**
   * Constructor for Login component
   */
  constructor() {
    this.templateUrl = 'commons/login/login.html';
    this.controller = LoginController;
  }
}

export default new LoginComponent();
