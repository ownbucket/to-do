/**
 * Login class.
 */
class LoginRoutes {
  /**
   * Constructor for LoginRoutes.
   * @param {Object} $stateProvider angular $state provider
   */
  constructor($stateProvider) {
    $stateProvider
      .state('login', {
        url: '^/login',
        template: `<login></login>`
      });
  }
}

const LoginRoutesFactory = $stateProvider => new LoginRoutes($stateProvider);

LoginRoutesFactory.$inject = ['$stateProvider'];

export default LoginRoutesFactory;
