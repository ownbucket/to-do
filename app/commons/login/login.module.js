import LoginRoutes from './login.routes';
import LoginComponent from './login.component';

/**
 * @ngdoc overview
 * @name LoginModule
 * @requires ui.router
 * @description
 *
 * The `LoginModule` containes the login component and logic for the user to
 * sign into the application.
 */
export const LoginModule =
  angular
  .module('login', ['ui.router'])
  .component('login', LoginComponent)
  .config(LoginRoutes)
  .name;
