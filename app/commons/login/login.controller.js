/**
 * Login controller
 */
class LoginController {
  /**
   * Constructor for Login controller
   * @param {Object} $state angular $state service
   * @param {CloudService} cloudService CloudService
   */
  constructor($state, cloudService) {
    this.$state_ = $state;
    this.cloudService_ = cloudService;
  }

  /**
   * Log in
   */
   login() {
     this.cloudService_.signIn()
      .then(() => this.$state_.go('todo'));
   }
}

LoginController.$inject = [ '$state', 'CloudService' ];

export { LoginController };
