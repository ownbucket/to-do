/**
 * Header controller
 */
class HeaderController {
  /**
   * Constructor for Header controller
   * @param {Object} $state angular $state service
   * @param {CloudService} cloudService CloudService
   */
  constructor($state, cloudService) {
    this.$state = $state;
    this.cloudService_ = cloudService;
  }

  /**
   * Goes to state.
   */
  go(state) {
    const forceReloadParam = state === 'todo' ? { reload: true } : {};
    this.$state.go(state, {}, forceReloadParam);
  }

  /**
   * Logs out.
   */
  logout() {
    this.cloudService_.signOut().then(() => this.go('login'));
  }
}

HeaderController.$inject = [ '$state', 'CloudService' ];

export { HeaderController };
