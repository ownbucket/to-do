import { HeaderController } from './header.controller';

/**
 * Header Component.
 */
class HeaderComponent {
  /**
   * Constructor for Header component
   */
  constructor() {
    this.templateUrl = 'commons/header/header.html';
    this.controller = HeaderController;
  }
}

export default new HeaderComponent();
