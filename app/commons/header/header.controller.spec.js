import { HeaderController } from './header.controller';

let stateMock;
let cloudServiceMock;
let headerController;

describe('HeaderController', () => {

  beforeEach(() => {
    stateMock = jasmine.createSpyObj('stateMock', ['go']);
    cloudServiceMock = jasmine.createSpyObj('cloudServiceMock', ['signOut']);
    headerController = new HeaderController(stateMock, cloudServiceMock);
  });

  describe('contructor ', () => {
    it('should be defined', () => {
      expect(headerController).toBeDefined();
      expect(headerController.$state).toBeDefined();
      expect(headerController.cloudService_).toBeDefined();
    });
  });

});
