import { MainModule } from './main/main.module';
import { LoginModule } from './login/login.module';
import HeaderComponent from './header/header.component';

/**
 * @ngdoc overview
 * @name CommonsModule
 * @requires MainModule
 * @requires LoginModule
 * @description
 *
 * The `CommonsModule` module bundles the specific components of the application
 * that are not intended to use in another application.
 */
export const CommonsModule =
  angular
    .module('commons', [
      MainModule,
      LoginModule
    ])
    .component('header', HeaderComponent)
    .name;
