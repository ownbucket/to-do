/**
 * Main class.
 */
class MainRoutes {
  /**
   * Constructor for MainRoutes.
   * @param {Object}  $stateProvider angular $state provider
   * @returns {void}
   */
  constructor($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        resolve: {
          init: ['CloudService', CloudService => CloudService.init()]
        }
      });
  }
}

const MainRoutesFactory = $stateProvider => new MainRoutes($stateProvider);

MainRoutesFactory.$inject = ['$stateProvider'];

export default MainRoutesFactory;
