import MainRoutes from './main.routes';

/**
 * @ngdoc overview
 * @name MainModule
 * @requires ui.router
 * @description
 *
 * The `MainModule` module works as root recipe for the app. It provides a main
 * state to wrapp the application substates.
 */
export const MainModule =
  angular
  .module('main', ['ui.router'])
  .config(MainRoutes)
  .name;
