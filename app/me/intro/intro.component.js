import { IntroController } from './intro.controller';

/**
 * Intro component.
 */
class IntroComponent {
  /**
   * Constructor for Intro Component.
   */
  constructor() {
    this.templateUrl = 'me/intro/intro.html';
    this.controller = IntroController;
  }
}

export default new IntroComponent();
