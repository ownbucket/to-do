/**
 * Intro controller
 */
class IntroController {
  /**
   * Constructor for Intro controller.
   * @param {Object} $state angular $state service
   */
  constructor($state) {
    this.$state = $state;
  }
}

IntroController.$inject = [ '$state' ];

export { IntroController };
