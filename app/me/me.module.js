import IntroComponent from './intro/intro.component';
import Routes from './me.routes';

/**
 * @ngdoc overview
 * @name MeModule
 * @requires ui.router
 * @description
 *
 * The `MeModule` module is the container reference for me as the author of the
 * application.
 */
export const MeModule =
  angular
  .module('me', ['ui.router'])
  .component('meIntro', IntroComponent)
  .config(Routes)
  .name;
