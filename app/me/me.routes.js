/**
 * Me class.
 */
class MeRoutes {
  /**
   * Constructor for MeRoutes.
   * @param {Object} $stateProvider angular $state provider
   * @returns {void}
   */
  constructor($stateProvider) {
    $stateProvider
      .state('me', {
        url: '^/me',
        template: `<me-intro></me-intro>`
      })
  }
}

const MeRoutesFactory = $stateProvider => new MeRoutes($stateProvider);

MeRoutesFactory.$inject = ['$stateProvider'];

export default MeRoutesFactory;
