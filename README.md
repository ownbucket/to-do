
# to-do-app

---

## Content

  1. [Introduction](#introduction)
  1. [Trial-Demo](#trial-demo)
  1. [Requirements and Installation](#reqs-installation)
  1. [Modularity & Code Ordering](#modularity-code)
  1. [Client - Server Implementation](#client-server)
  1. [Documentation (ngDocs)](#documentation)
  1. [Code Quality (jshint)](#code-quality)
  1. [Technologies Selection](#technologies-selection)
  1. [Tests (Jasmine)](#tests)
  1. [Patterns and Code Abstraction](#patterns)
  1. [Versioning](#versioning)
    1. [Branching](#branching)
    1. [Committing](#committing)
    1. [Tagging](#tagging)
  1. [File Naming](#file-naming)
  1. [Scaffolding](#scaffolding)
  1. [Styles](#styles-guide)

---

## Introduction

  ...

## Trial-Demo

  The aim of this repo is to give a tour around some technical abilities of
  Rodrigo Benavides as a Web UI Developer.

---

## Requirements and Installation

  ...

---

## Modularity & Code Ordering

The project is mainly ordered in components and has taken advantage of ES6 to
implement modularity in the app. Modularity allows us to scale, maintain and
well structure large applications.
Main components are bundled in modules. Each module encapsulates the logic,
templates, routing, and child components.
The project is organized in three main modules:

* Main module

This module sets de main state where the entire application is based on.

* Component module

Its the module for all the reusable components. This module with all its
components can be used in another application

* Common module

This is the container for all the components for our specific application.
These can be footers, navigation or layout components.

---

## Client - Server Implementation

  * The client web app is built up with AngularJS 1.6

---

**[Back to top](#content)**

## Documentation (ngDocs)

The documentation of this AngularJS project is implemented with jsdoc inline
annotations type. NgDocs generates the proper documentation for the project.
To build the documentation based on ngDocs annotations, there is a gulp task
which has to be run with:

  - npm run ngdocs

which launches the gulp task for ngdocs

Setup:

  - npm install gulp-ngdocs --save-dev

The documentation can be addressed in http://localhost:8091/

---

## Code Quality (ESLint)

ESLint is the linting utility for the JavaScript files in this project.
The .eslintrc configuration files includes the most recommended rules to solve
common problems when coding as the related with:

* ES6 presets
* variables implementation
* syntax or logic errors
* best practices

Setup:

  - npm install eslint --save-dev

---

## Technologies Selection

For a better understanding, the technologies used in this project are divided
in the next four categories:

---

### Developing Dependencies

* npm - package manager and launcher of tasks
* Gulp - build system automating tasks
* Webpack - module bundler to serve static assets for the browser
* ESLint - JavaScript files linter to avoid errors
* Sass - extension of CSS which gives elegant and scalable features to normal CSS
* Babel - JavaScript compiler
* Lodash - JavaScript utility library delivering modularity, performance & extras
* Git - Management of versions

---

**[Back to top](#content)**

### Testing Dependencies

* Jasmine - behavior-driven development framework for testing JavaScript code
* Karma - test runner for JavaScript

---

### Client environment

* AngularJS 1.6 - Google framework to build SPA with JavaScript
* Angular Material - UI Component framework


---

## Tests (Jasmine, Protractor)

...

---

## Patterns and Code Abstraction

...

---

**[Back to top](#content)**

## Versioning

To record changes and get an well organized version control, this app uses
Git.

### About the Branching

Usually large apps implement at least 3 different levels of branches: production,
test, development. However this app makes use of only 2:

* master - for final versions or releases
* development - for constant development

For local branches, the naming convention is the following:

* Feature Branches
Used when developing a new feature or enhancing existing code which will take
long time.
Naming: feat-<tbd descriptor>

 - $ git checkout -b feat-id..

* Bug Branches
Bug branches differ from feature branches only semantically. Bug branches will
be created when there is a bug on the live site that should be fixed and merged
into the next deployment.
Naming: bug-<tbd descriptor>

-  $ git checkout -b bug-id..

* Hotfix Branches
A hotfix branch comes from the need to act immediately upon an undesired state
of a live production version.
Naming: hot-<tbd descriptor>

- $ git checkout -b hot-id..

### Committing

* New features
feat(file or files name): commit description

* Fixes
fix(file or files name): commit description

### Tagging

To mark important changes in the developing history this project uses Git Tagging.

> Useful commands:

```
  /*To list available tags*/
  $ git tag
  v0.1
  v1.3

  /* To list specific tags*/
  $ git tag -l "v1.8.5*"
  v1.8.5
  v1.8.5-rc0
  v1.8.5-rc1

  /*To see data in the commit that was tagged. Shows
  only commit info when the tag is lightweighted type*/
  $ git show v1.4
  tag v1.4
  Tagger: Ben Straub <ben@straub.cc>
  Date:   Sat May 3 20:19:12 2014 -0700
  commit ca82a6dff817ec66f44342007202690a93763949
  Author: Scott Chacon <schacon@gee-mail.com>
  Date:   Mon Mar 17 21:52:11 2008 -0700

```

> To create tags

Annotated Tags

Contains name, email, and date

```
  $ git tag -a v1.4 -m "my version 1.4"
  $ git tag
  v0.1
  v1.3
  v1.4
```

Lightweight Tags

It’s a pointer to a specific commit

```
  $ git tag v1.4-lw
  $ git tag
  v0.1
  v1.3
  v1.4
  v1.4-lw
  v1.5
```

> Semantic Versioning

Following the semantic versioning in http://semver.org/ the next standards are
followed in this application:

Given a version number MAJOR.MINOR.PATCH, increment the:

* MAJOR version when you make incompatible API changes,
* MINOR version when you add functionality in a backwards-compatible manner, and
* PATCH version when you make backwards-compatible bug fixes.

---

## File Naming

For the file naming convention it is used lowercase, dashes separated name,
and type of file.

```javascript
  test.module.js
  test.component.js
  test.service.js
  test.directive.js
  test.filter.js
  test.routes.js
  test.[type-of-file].spec.js
  test.html
  test.scss
```
---

**[Back to top](#content)**

## Scaffolding

> Folders Tree
```
    |-- app/
    |   |-- config/
    |   |   |-- app.config.js
    |   |   |-- app.data.js
    |   |-- components/
    |   |   |-- test/
    |   |   |   |-- test.module.js
    |   |   |   |-- test.component.js
    |   |   |   |-- test.controller.js
    |   |   |   |-- test.controller.spec.js
    |   |   |   |-- test.service.js
    |   |   |   |-- test.service.spec.js
    |   |   |   |-- test.router.js
    |   |   |   |-- test.html
    |   |   |   |-- test.scss
    |   |   |-- ...
    |   |   |-- components.module.js
    |   |-- commons/
    |   |   |-- test/
    |   |   |   |-- test.module.js
    |   |   |   |-- test.component.js
    |   |   |   |-- test.controller.js
    |   |   |   |-- test.controller.spec.js
    |   |   |   |-- test.service.js
    |   |   |   |-- test.service.spec.js
    |   |   |   |-- test.router.js
    |   |   |   |-- test.html
    |   |   |   |-- test.scss
    |   |   |-- ...
    |   |   |-- commons.module.js
    |   |-- styles/
    |   |   |-- utils/
    |   |   |   |-- mixins.scss
    |   |   |   |-- variables.scss
    |-- app.js
    |-- index.html
    |-- favicon.ico
```

---

## Styles

Styles are written with Sassy CSS syntax (SCSS) and they follow the class naming
convention of BEM.
BEM (Block, Element, Modifier) methodology is intended to describe better the HTML
elements with its CSS styles in large apps.

```css
  /* Block component */
  .btn {}

  /* Element that depends upon the block */
  .btn__price {}

  /* Modifier that changes the style of the block */
  .btn--selected {}
  .btn--disabled {}
  .btn--small {}
```
