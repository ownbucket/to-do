const webpack = require('webpack');
const karmaConfig =

module.exports = config =>
  config.set({
    browsers:   ['PhantomJS'],
    frameworks: ['jasmine'],
    reporters:  ['spec', 'coverage'],
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: false,
    colors: true,
    port: 9876,
    specReporter: {
      maxLogLines: 5
    },
    basePath: '',
    files: ['webpack.karma.context.js'],
    preprocessors: {
      'webpack.karma.context.js': ['webpack']
    },
    coverageReporter: {
      type : 'html',
      dir: 'coverage',
      subdir : 'report'
    },
    exclude: [],
    webpack: {
      devtool: 'eval',
      module: {
        loaders: [
          {test: /\.js$/, loader: 'babel', exclude: /(\.test.js$|node_modules)/},
          {test: /\.css$/, loader: 'style!css'},
          {test: /\.scss$/, loader: 'css!sass'},
          {test: /\.html/, loader: 'raw'},
          {test: /\.json/, loader: 'json'},
          {test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/, loader: 'url?limit=50000'}
        ]
      },
      plugins: [
      ]
    },
    webpackMiddleware: {
      noInfo: true
    }
  });
