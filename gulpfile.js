
'use strict';

const gulp = require('gulp');

/**
 *  Gulp plugins.
 */
const plugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'load-gulp-tasks', 'del'],
  lazy: true
});

/**
 *  Loads js files.
 */
plugins.loadGulpTasks(gulp, {}, plugins);

/**
 *  Default gulp task.
 */
gulp.task('default', ['test', 'lint'], () => gulp.start('build'));
