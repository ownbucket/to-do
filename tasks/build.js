/**
 * Task that creates the distribution folder
 */

'use strict';


module.exports = (gulp, options, plugins) => {

  /**
   * Main build task
   */
  gulp.task('build', [], () => gulp.start('build:dev'));

  /**
   * Build for development task
   */
  gulp.task('build:dev', ['build:clean'], () => gulp.start('webpack:dev'));

  /**
   * Removes the dist and .tmp folder
   */
  gulp.task('build:clean', done => {
    plugins.del([options.paths.dist]);
    done();
  });

};
