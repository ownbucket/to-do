/**
 * Runs Webpack.
 */

'use strict';

module.exports = (gulp, options, plugins) => {

  const webpack = require('webpack');
  const webpackConfig = require('../webpack.config');

  /**
   * Webpack task.
   */
  gulp.task('webpack:dev', callback => webpack(webpackConfig, (err, stats) => {
    if (err) {
      throw new plugins.util.PluginError('webpack:build', err);
    }
    plugins.util.log('[webpack]', stats.toString({
      colors: true,
      chunks: false,
      version: false,
      hash: false
    }));
    return callback();
  }));

};
