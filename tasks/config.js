/**
 * Asigns paths for the application.
 */

'use strict';

const defaultPaths = {
  src: 'app',
  dist: 'public',
  tmp: '.tmp',
  ngdocs: 'ngdocs',
  coverage: 'coverage'
};

module.exports = (gulp, options) => options.paths = defaultPaths;
