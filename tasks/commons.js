/**
 * Exposes common error handlers
 */

'use strict';

const util = require('gulp-util');
const errorCallback = err => {
  util.log(util.colors.red('[' + title + ']'), err.toString());
  this.emit('end');
};

module.exports.errorHandler = title => errorCallback;
