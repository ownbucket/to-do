/**
 * Task that loads karma configuration
 */

'use strict';

module.exports = (gulp, options, plugins) => {

  const KarmaServer = require('karma').Server;
  const karmaConfigFile = __dirname + '/../karma.conf.js';
  const karmaTddConf = {
    configFile: karmaConfigFile
  };
  const karmaTestConf = {
    configFile: karmaConfigFile,
    singleRun: true
  };
  const servConf = {
    root: options.paths.coverage + '/report',
    livereload: true,
    fallback: options.paths.coverage + '/report/index.html',
    port: 9877
  };

  /**
   * Runs and wathces unit tests.
   */
  gulp.task('tdd', () =>
    new KarmaServer(karmaTddConf, plugins.connect.server(servConf)).start()
  );

  /**
   * Runs test once.
   */
  gulp.task('test', done => new KarmaServer(karmaTestConf, done).start());

};
