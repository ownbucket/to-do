/**
 * Task that creates the documentation
 */
'use strict';

module.exports = (gulp, options, plugins) => {

  const optionsDocs = {
    html5Mode: true,
    title: "ToDo App Documentation",
    startPage: '/api',
    api: {
      src: ['app/**/*.js', '!app/**/*-spec.js'],
      title: 'Docs'
    }
  };
  const servConf = {
    root: options.paths.ngdocs,
    livereload: true,
    fallback: options.paths.ngdocs + '/index.html',
    port: 8091
  };

  /**
   * Creates ngDocs files
   */
  gulp.task('ngdocs:build', ['ngdocs:clean'], () =>
    gulp
      .src('./app/**/*.js')
      .pipe(plugins.ngdocs.process(optionsDocs))
      .pipe(gulp.dest(options.paths.ngdocs))
  );

  /**
   * Removes the ngdocs folder
   */
  gulp.task('ngdocs:clean', done => {
    plugins.del([options.paths.ngdocs]);
    done();
  });

  /**
   * Serves ngdocs
   */
  gulp.task('ngdocs', ['ngdocs:build'], () => plugins.connect.server(servConf));

};
