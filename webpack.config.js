/**
 * Webpack config.
 * */

var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var config = {

  devtool: '#inline-source-map',

  entry: {
    bundle: './app/app.js',
    vendor: [
      'angular',
      'angular-ui-router',
      'angular-animate',
      'angular-aria',
      'angular-sanitize',
      'angular-messages',
      'angular-material',
      'jquery',
      'lodash'
    ],
    styles: [
      'normalize.css/normalize.css',
      'angular-material/angular-material.css',
      './app/styles/index.scss'
    ]
  },

  module: {
    preLoaders: [
      {
        test: /app.*\.js$/,
        exclude: '/node_modules/',
        loader: 'eslint-loader'
      }
    ],
    loaders: [
      {
        test: /app.*\.js$/,
        exclude: '/node_modules/',
        loaders: [
          'babel-loader'
        ]
      }, {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css!sass')
      }, {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
      }, {
        test: /\.html$/,
        loader: 'raw'
      }, {
        test: /\.json/,
        loader: 'json'
      }
    ]
  },

  output: {
    path: path.resolve(__dirname, 'public/'),
    filename: 'assets/[name].js'
  },

  plugins: [
    new ExtractTextPlugin('assets/[name].css')
  ],

  devServer: {
    port: 8000,
    historyApiFallback: true,
    contentBase: './app',
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunks: false
    }
  }
};

module.exports = config;
